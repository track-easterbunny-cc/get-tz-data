# track.easterbunny.cc Get TZ Data Script Version 1.0.0
# Copyright (C) 2021 track.easterbunny.cc

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import requests
import csv
import time

processingtime = "1617408000"
apikey = ""

with open("route.tsv") as fc, open("timezones.csv", 'w', newline='') as tzdata:
    tsv_reader = csv.reader(fc, delimiter="\t", quotechar='"')
    csv_writer = csv.writer(tzdata)
    csv_writer.writerow(["Timezone"])
    progress = 1
    for row in tsv_reader:
        print("Now processing row: %s" % str(progress))
        try:
            float(row[9])
            float(row[10])
        except ValueError:
            print("No lat/lng for this row...")
            continue
            
        while True:
            tzrequest = requests.get("https://maps.googleapis.com/maps/api/timezone/json?location=%s,%s&timestamp=%s&key=%s" % (str(row[9]), str(row[10]), processingtime, apikey))
            tzrequest_json = tzrequest.json()
            tzregion = ""
            try:
                tzregion = tzrequest_json['timeZoneId']
                break
            except KeyError:
                print("TZ Data not found here - This is likely due to bad API key propagation.")
                time.sleep(0.5)

        csv_writer.writerow([tzregion])
        progress = progress + 1

print("All done!")