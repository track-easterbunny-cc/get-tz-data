# Note: Repository deprecated
The Get TZ Data repository has now been deprecated, as this script has been wrapped into the route compiler built into the TEBCC main repository.

Nonetheless, this code will remain online for anyone who wants to use it.

# Get TZ Data

Script to grab timezones for stops with the Google Maps Timezone API for use in a route file.

# Getting started
To use the Get TZ Data script, you'll need:
* A v5-style route file in TSV format, with the proper latitude/longitude fields filled out
* Python 3
* A Google Maps API key that has access to the Time Zone API
* The requests library (`pip3 install requests`)

To use, download this repository using Git or GitLab. Then, put your v5-style route file in the directory as `route.tsv`. This route **needs** to have the latitude and longitude field filled out in the proper positions.

# Usage
There's two configuration variables in `main.py` that you need to configure before running this script.

The first one, `processingtime` (at line 5), is the UNIX timestamp for when you'll be requesting timezone data. You'll want this to be at the start of your tracker run. Use a site like epochconverter.com to find the UNIX timestamp for when your tracker run starts.

The second one, `apikey` (at line 6), is your API key for the Google Maps Time Zone API. Make sure this key isn't restricted in any way that wouldn't allow the requests to go through.

Once complete, run `main.py`. It should take about 1 minute per 100 timezone requests - but this is largely dependent on your internet connection (especially your latency).

## Note about API key propagation
After generating a new API key, you'll likely run into issues where your API key is accepted for some (but not all) requests. If this happens, the script will stop for 0.5 seconds before retrying.

If you just generated your API key, this can drastically increase the time it takes to get all timezone data. We recommend waiting about 15-30 minutes after generating an API key just so it propagates properly.

Once the script is complete, a file named `timezones.csv` will be generated. Import that file into software like Google Sheets/Excel, then copy & paste the contents of the one column from `timezones.csv` into your main route file for the timezone field.

# Licensing
The Get TZ Data script is licensed under the MIT License.

# Contributing
We welcome improvements to the Get TZ Data script. It's rough since it's brand new to the tracker as of v5 - and we'd love some help cleaning it up.

One improvement we hopefully will do for v6 - the script should be updated so it's grabbing the timezone data for each stop's arrival time. If you'd like to submit a PR with this fix, please go ahead and do so!

Otherwise, fork the repo, make some changes, and make a PR. You can also propose changes in an issue before you get started.
