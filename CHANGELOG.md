# track.easterbunny.cc Get TZ Data Changelog
This is the changelog for the track.easterbunny.cc Get TZ Data script. It details all the changes made in releases of the Get TZ Data script.

# Version 1.0.0 (track.easterbunny.cc v5.5.1)
* The Get TZ Data script is now open source!
